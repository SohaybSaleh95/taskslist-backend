<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $primaryKey = 'task_id';

    public function category(){
        return $this->belongsTo('App\Category','category_id','category_id');
    }
}
