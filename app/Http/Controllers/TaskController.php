<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Response;
use App\Task;

use Validator;

class TaskController extends Controller
{
    /**
     * HTTP Put
     * 
     * creates new Task
     * 
     * @param string $tutle
     * 
     * @param integer $category_id
     * 
     * @param string $description
     * 
     * @param date due_date
     * 
     * @param string $tags (optional)
     * 
     * @return Task
     */
    public function create(Request $request){
        $rr = new Response();

        $val = Validator::make($request->all(),[
            'title' => 'required',
            'category_id' => 'required|integer|exists:categories',
            'description' => 'required',
            'due_date' => 'required|date',
            'tags' => 'nullable'
        ]);

        if($val->fails()){
            $rr->data = $val->errors();
            $rr->setBadRequest();
            return $rr->getResult();
        }
        extract($request->all());

        $task = new Task();
        $task->category_id = $category_id;
        $task->title = $title;
        $task->description = $description;
        $task->tags = $tags;
        $task->due_date = date('Y-m-d H:i:s',strtotime($due_date));
        $task->save();

        $rr->setSuccess('The task was added succesfully!');
        $rr->data = $task;
        return $rr->getResult();
    }

    /**
     * Http Post
     * 
     * @param integer task_id
     * 
     * @param string title
     * 
     * @param integer category_id
     * 
     * @param string description
     * 
     * @param date due_date
     * 
     * @param string tags
     * 
     * @return Task 
     */
    public function update(Request $request){
        $rr = new Response();

        $val = Validator::make($request->all(),[
            'task_id' => 'required|integer|exists:tasks',
            'title' => 'required',
            'category_id' => 'required|integer|exists:categories',
            'description' => 'required',
            'due_date' => 'required|date',
            'tags' => 'nullable'
        ]);

        if($val->fails()){
            $rr->setBadRequest();
            $rr->data = $val->errors();
            return $rr->getResult();
        }

        extract($request->all());

        $task = Task::find($task_id);
        $task->title = $title;
        $task->category_id = $category_id;
        $task->description = $description;
        $task->tags = $tags;
        $task->due_date = date('Y-m-d H:i:s',strtotime($due_date));
        $task->save();

        $rr->data = $task;
        $rr->setSuccess("The task was updated successfully!");
        return $rr->getResult();
    }

    /**
     * Http Get
     * 
     * @param integer task_id (optional)
     * 
     * @return Task/Task[]
     */
    public function view(Request $request,$task_id = 0){
        $rr = new Response(false);
        if($task_id == 0){
            $rr->data = Task::all();
        }else{
            $category = Category::find($task_id);
            
            if($category == null){
                $rr->setNotFound();
                return $rr->getResult();
            }

            $rr->data = $category;
        }
        return $rr->getResult();
    }

    /**
     * Http Delete
     * 
     * @param integer task_id
     * 
     * @return integer old_task_id
     */
    public function delete(Request $request,$task_id){
        $rr = new Response();
        
        $task = Task::find($task_id);
        if($task == null){
            $rr->setNotFound();
            return $rr->getResult();
        }

        $task->delete();
        $rr->data = $task_id;
        
        $rr->setSuccess('The task was deleted successfully!');

        return $rr->getResult();
    }
}
