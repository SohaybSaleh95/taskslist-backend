<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Response;
use App\Category;
use App\Task;

use Validator;

class CategoryController extends Controller
{
    /**
     * HTTP Put
     * 
     * creates new category
     * 
     * @param  string  $name
     * 
     * @return Category
     */
    public function create(Request $request){
        $rr = new Response();

        $val = Validator::make($request->all(),[
            'name' => 'required'
        ]);

        if($val->fails()){
            $rr->data = $val->errors();
            $rr->setBadRequest();
            return $rr->getResult();
        }
        extract($request->all());

        $category = new Category();
        $category->name = $name;
        $category->save();

        $rr->setSuccess('The category was added succesfully!');
        $rr->data = $category;
        return $rr->getResult();
    }

    /**
     * Http Post
     * 
     * @param integer category_id
     * 
     * @param string name
     * 
     * @return Category 
     */
    public function update(Request $request){
        $rr = new Response();

        $val = Validator::make($request->all(),[
            'category_id' => 'required|integer|exists:categories',
            'name' => 'required'
        ]);

        if($val->fails()){
            $rr->setBadRequest();
            $rr->data = $val->errors();
            return $rr->getResult();
        }

        extract($request->all());

        $category = Category::find($category_id);
        $category->name = $name;
        $category->save();

        $rr->data = $category;
        $rr->setSuccess("The category was updated successfully!");
        return $rr->getResult();
    }

    /**
     * Http Get
     * 
     * @param integer category_id (optional)
     * 
     * @return Category/Category[]
     */
    public function view(Request $request,$category_id = 0){
        $rr = new Response(false);
        if($category_id == 0){
            $rr->data = Category::all();
        }else{
            $category = Category::with('tasks')->find($category_id);
            
            if($category == null){
                $rr->setNotFound();
                return $rr->getResult();
            }

            $rr->data = $category;
        }
        return $rr->getResult();
    }

    /**
     * Http Delete
     * 
     * @param integer category_id
     * 
     * @return integer old_category_id
     */
    public function delete(Request $request,$category_id){
        $rr = new Response();
        
        $category = Category::find($category_id);
        if($category == null){
            $rr->setNotFound();
            return $rr->getResult();
        }

        //$category->tasks->delete();
        $category->delete();
        $rr->data = $category_id;
        
        $rr->setSuccess('The category was deleted successfully!');

        return $rr->getResult();
    }

    /**
     * Http Get
     * 
     * @param integer category_id
     * 
     * @return Task[]
     */
    public function tasks(Request $request,$category_id){
        $rr = new Response(false);
        $rr->data = Task::where('category_id',$category_id)->get();
        return $rr->getResult();
    }
}
