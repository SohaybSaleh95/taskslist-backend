<?php

namespace App;

class Response{
    public $show_success_message = true;
    public $message = "The operation was completed successfully!";
    public $success = true;
    public $data = [];
    public $code = 200;

    public function __construct($show_message = true){
        $this->show_success_message = $show_message;
    }

    public function setSuccess($msg = "The operation was completed successfully!"){
        $this->message = $msg;
        $this->code = 200;
    }

    public function setBadRequest($msg = "The data you enter is invalid"){
        $this->message = $msg;
        $this->success = false;
        $this->code = 400;
    }

    public function setNotFound(){
        $this->message = "The request you are trying to reach cant be found";
        $this->success = false;
        $this->code = 404;
    }

    public function setFail($msg = "An error occured while proccessing your request!"){
        $this->message = $msg;
        $this->success = false;
        $this->code = 500;
    }

    public function getResult(){
        return response()->json($this,$this->code);
    }
}