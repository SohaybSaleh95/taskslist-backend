<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => '/categories','middleware' => 'CORS'],function(){
    Route::put('/','CategoryController@create');
    Route::post('/','CategoryController@update');
    Route::get('/{category_id?}','CategoryController@view');
    Route::delete('/{category_id}','CategoryController@delete');
    Route::get('/{category_id}/tasks','CategoryController@tasks');

    Route::group(['prefix' => '/tasks'],function(){
        Route::put('/','TaskController@create');
        Route::post('/','TaskController@update');
        Route::get('/{category_id?}','TaskController@view');
        Route::delete('/{category_id}','TaskController@delete');
    });
});
